package avila.daniel.calculator.domain.interactor

import app.cash.turbine.test
import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.SUBTRACT_RESULT
import avila.daniel.calculator.domain.operands
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class SubtractUseCaseTest {

    private lateinit var subtractUseCase: SubtractUseCase
    private lateinit var calc: ICalc

    @Before
    fun setUp() {
        calc = Mockito.mock(ICalc::class.java)
        subtractUseCase = SubtractUseCase(calc)
    }

    @Test
    fun `should subtract two numbers`() = runBlocking {
        Mockito.`when`(calc.subtract(operands)).thenReturn(
            flow {
                emit(SUBTRACT_RESULT)
            }
        )

        subtractUseCase.execute(operands).test {
            assertEquals(SUBTRACT_RESULT, awaitItem())
            awaitComplete()
        }

        Mockito.verify(calc, Mockito.times(1)).subtract(operands)
        Mockito.verifyNoMoreInteractions(calc)
    }
}