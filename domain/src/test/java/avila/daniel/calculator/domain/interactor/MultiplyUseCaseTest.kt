package avila.daniel.calculator.domain.interactor

import app.cash.turbine.test
import avila.daniel.calculator.domain.*
import junit.framework.Assert
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class MultiplyUseCaseTest {

    private lateinit var multiplyUseCase: MultiplyUseCase
    private lateinit var calc: ICalc

    @Before
    fun setUp() {
        calc = Mockito.mock(ICalc::class.java)
        multiplyUseCase = MultiplyUseCase(calc)
    }

    @Test
    fun `should multiply two numbers`() = runBlocking {
        Mockito.`when`(calc.multiply(operands)).thenReturn(
            flow {
                emit(MULTIPLY_RESULT)
            }
        )

        multiplyUseCase.execute(operands).test {
            Assert.assertEquals(MULTIPLY_RESULT, awaitItem())
            awaitComplete()
        }

        Mockito.verify(calc, Mockito.times(1)).multiply(operands)
        Mockito.verifyNoMoreInteractions(calc)
    }
}