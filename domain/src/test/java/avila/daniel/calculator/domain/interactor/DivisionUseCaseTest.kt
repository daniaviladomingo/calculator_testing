package avila.daniel.calculator.domain.interactor

import app.cash.turbine.test
import avila.daniel.calculator.domain.*
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class DivisionUseCaseTest {

    private lateinit var divisionUseCase: DivisionUseCase
    private lateinit var calc: ICalc

    @Before
    fun setUp() {
        calc = Mockito.mock(ICalc::class.java)
        divisionUseCase = DivisionUseCase(calc)
    }

    @Test
    fun `should division two numbers`() = runBlocking {
        Mockito.`when`(calc.division(operands)).thenReturn(
            flow {
                emit(DIVISION_RESULT)
            }
        )

        divisionUseCase.execute(operands).test {
            assertEquals(DIVISION_RESULT, awaitItem())
            awaitComplete()
        }

        Mockito.verify(calc, Mockito.times(1)).division(operands)
        Mockito.verifyNoMoreInteractions(calc)
    }
}