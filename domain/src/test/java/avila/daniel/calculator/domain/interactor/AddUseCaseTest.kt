package avila.daniel.calculator.domain.interactor

import app.cash.turbine.test
import avila.daniel.calculator.domain.ADD_RESULT
import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.operands
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class AddUseCaseTest {

    private lateinit var addUseCase: AddUseCase
    private lateinit var calc: ICalc

    @Before
    fun setUp() {
        calc = Mockito.mock(ICalc::class.java)
        addUseCase = AddUseCase(calc)
    }

    @Test
    fun `should add two numbers`() = runBlocking {
        Mockito
            .`when`(calc.add(operands))
            .thenReturn(
                flow {
                    emit(ADD_RESULT)
                }
            )

        addUseCase.execute(operands).test {
            assertEquals(ADD_RESULT, awaitItem())
            awaitComplete()
        }

        Mockito.verify(calc, Mockito.times(1)).add(operands)
        Mockito.verifyNoMoreInteractions(calc)
    }
}