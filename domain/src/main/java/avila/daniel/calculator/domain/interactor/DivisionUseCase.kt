package avila.daniel.calculator.domain.interactor

import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.interactor.type.SingleUseCaseWithParameter
import avila.daniel.calculator.domain.model.Operands
import kotlinx.coroutines.flow.Flow

class DivisionUseCase(private val calc: ICalc) : SingleUseCaseWithParameter<Operands, Float> {
    override fun execute(parameter: Operands): Flow<Float> = calc.division(parameter)
}