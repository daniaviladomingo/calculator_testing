package avila.daniel.calculator.domain

import avila.daniel.calculator.domain.model.Operands
import kotlinx.coroutines.flow.Flow

interface ICalc {
    fun add(ops: Operands): Flow<Float>
    fun subtract(ops: Operands): Flow<Float>
    fun multiply(ops: Operands): Flow<Float>
    fun division(ops: Operands): Flow<Float>
}