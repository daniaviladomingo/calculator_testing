package avila.daniel.calculator.domain.interactor.type

import kotlinx.coroutines.flow.Flow

interface SingleUseCaseWithParameter<P, R> {
    fun execute(parameter: P): Flow<R>
}
