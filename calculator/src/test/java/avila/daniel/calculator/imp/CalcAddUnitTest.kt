package avila.daniel.calculator.imp

import app.cash.turbine.test
import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.model.Operands
import avila.domingo.calculator.imp.CalcImp
import org.junit.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class CalcAddUnitTest {
    private val calc: ICalc = CalcImp()

    @Test
    fun `add with +op1 & +op2 is correct`() = runBlocking {
        calc.add(Operands(2f, 2f)).test {
            assertEquals(4f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `add with -op1 & +op2 is correct`() = runBlocking {
        calc.add(Operands(-2f, 2f)).test {
            assertEquals(0f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `add with +op1 & -op2 is correct`() = runBlocking {
        calc.add(Operands(2f, -2f)).test {
            assertEquals(0f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `add with -op1 & -op2 is correct`() = runBlocking {
        calc.add(Operands(-2f, -2f)).test {
            assertEquals(-4f, awaitItem())
            awaitComplete()
        }
    }
}
