package avila.daniel.calculator.imp

import app.cash.turbine.test
import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.model.Operands
import avila.domingo.calculator.imp.CalcImp
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(JUnit4::class)
class CalcSubtractUnitTest {
    private val calc: ICalc = CalcImp()

    @Test
    fun `subtract with +op1 & +op2 is correct`() = runBlocking {
        calc.subtract(Operands(2f, 2f)).test {
            Assert.assertEquals(0f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `subtract with +op1 & -op2 is correct`() = runBlocking {
        calc.subtract(Operands(2f, -2f)).test {
            Assert.assertEquals(4f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `subtract with -op1 & +op2 is correct`() = runBlocking {
        calc.subtract(Operands(-2f, 2f)).test {
            Assert.assertEquals(-4f, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `subtract with -op1 & -op2 is correct`() = runBlocking {
        calc.subtract(Operands(-2f, -2f)).test {
            Assert.assertEquals(0f, awaitItem())
            awaitComplete()
        }
    }
}
