package avila.domingo.calculator.imp

import avila.daniel.calculator.domain.ICalc
import avila.daniel.calculator.domain.model.Operands
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.IllegalArgumentException

class CalcImp : ICalc {
    override fun add(ops: Operands): Flow<Float> = flow { emit(ops.run { op1 + op2 }) }

    override fun subtract(ops: Operands): Flow<Float> = flow { emit(ops.run { op1 - op2 }) }

    override fun multiply(ops: Operands): Flow<Float> = flow { emit(ops.run { op1 * op2 }) }

    override fun division(ops: Operands): Flow<Float> = flow {
        if (ops.op2 == 0f) {
            throw IllegalArgumentException("Division by Zero")
        } else {
            emit(ops.run { op1 / op2 })
        }
    }
}