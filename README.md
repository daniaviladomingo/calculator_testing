![minSdkVersion 16](https://img.shields.io/badge/minSdkVersion-16-red.svg?style=true)
![compileSdkVersion 30](https://img.shields.io/badge/compileSdkVersion-30-yellow.svg?style=true)
[![Pipeline](https://gitlab.com/daniaviladomingo/calculator_testing/badges/master/pipeline.svg)](https://gitlab.com/daniaviladomingo/calculator_testing/-/pipelines)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daniaviladomingo_calculator_testing&metric=alert_status)](https://sonarcloud.io/dashboard?id=daniaviladomingo_calculator_testing)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=daniaviladomingo_calculator_testing&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=daniaviladomingo_calculator_testing)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=daniaviladomingo_calculator_testing&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=daniaviladomingo_calculator_testing)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=daniaviladomingo_calculator_testing&metric=coverage)](https://sonarcloud.io/dashboard?id=daniaviladomingo_calculator_testing)
[![codecov.io](https://codecov.io/gl/daniaviladomingo/calculator_testing/branch/master/graph/badge.svg)](https://app.codecov.io/gl/daniaviladomingo/calculator_testing)

[![License Apache 2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=true)](http://www.apache.org/licenses/LICENSE-2.0)



# Calculator app to implement testing feature

### Tools CI/CD, Code Quality
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Sonar](https://sonarcloud.io/)
- [Jacoco](https://www.eclemma.org/jacoco/)
    - [Gradle Project]()
    - [Android Gradle Project](https://github.com/arturdm/jacoco-android-gradle-plugin)
- [Codecov](https://about.codecov.io/)
    - [Upload report](https://github.com/codecov/codecov-bash)
- [Fastlane](https://fastlane.tools/)

### Languages, pattern and libraries used
- [Kotlin](https://kotlinlang.org/)
- [Clean Architecture](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
- [MVVM](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [Koin](https://github.com/InsertKoinIO/koin)
- [RxJava2](https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0)
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [JUnit](https://junit.org/junit5/)
- [Mockito](https://site.mockito.org/)
- [Espresso](https://developer.android.com/training/testing/espresso)
- [Robolectric](http://robolectric.org/)


![alt text](./Screenshot_20190423-165335.png)

## License

    Copyright 2019 Daniel Ávila Domingo

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
