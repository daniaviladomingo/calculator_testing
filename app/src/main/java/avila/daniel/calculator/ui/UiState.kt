package avila.daniel.calculator.ui

sealed class UiState {
    data class Result(val value: Float): UiState()
    object DivisionByZero: UiState()
}