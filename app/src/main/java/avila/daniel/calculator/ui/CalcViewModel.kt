package avila.daniel.calculator.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import avila.daniel.calculator.dispatcher.IDispatcherProvider
import avila.daniel.calculator.domain.interactor.AddUseCase
import avila.daniel.calculator.domain.interactor.DivisionUseCase
import avila.daniel.calculator.domain.interactor.MultiplyUseCase
import avila.daniel.calculator.domain.interactor.SubtractUseCase
import avila.daniel.calculator.domain.model.Operands
import avila.daniel.calculator.util.SingleLiveEvent
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class CalcViewModel(
    private val addUseCase: AddUseCase,
    private val divisionUseCase: DivisionUseCase,
    private val multiplyUseCase: MultiplyUseCase,
    private val subtractUseCase: SubtractUseCase,
    private val dispatcherProvider: IDispatcherProvider
) : ViewModel() {

    val resultLiveData = SingleLiveEvent<UiState>()

    fun add(op1: Float, op2: Float) {
        //resultLiveData.value = Resource.loading()
        viewModelScope.launch(dispatcherProvider.ui()) {
            addUseCase.execute(Operands(op1, op2))
                .flowOn(dispatcherProvider.io())
                .collect { result ->
                    resultLiveData.value = UiState.Result(result)
                }
        }
    }

    fun division(op1: Float, op2: Float) {
        //resultLiveData.value = Resource.loading()
        viewModelScope.launch(dispatcherProvider.ui()) {
            divisionUseCase.execute(Operands(op1, op2))
                .flowOn(dispatcherProvider.io())
                .catch {
                    resultLiveData.value = UiState.DivisionByZero
                }
                .collect { result ->
                    resultLiveData.value = UiState.Result(result)
                }
        }
    }

    fun multiply(op1: Float, op2: Float) {
        //resultLiveData.value = Resource.loading()
        viewModelScope.launch(dispatcherProvider.ui()) {
            multiplyUseCase.execute(Operands(op1, op2))
                .flowOn(dispatcherProvider.io())
                .collect { result ->
                    resultLiveData.value = UiState.Result(result)
                }
        }
    }

    fun subtract(op1: Float, op2: Float) {
        //resultLiveData.value = Resource.loading()
        viewModelScope.launch(dispatcherProvider.ui()) {
            subtractUseCase.execute(Operands(op1, op2))
                .flowOn(dispatcherProvider.io())
                .collect { result ->
                    resultLiveData.value = UiState.Result(result)
                }
        }
    }
}