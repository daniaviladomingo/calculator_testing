package avila.daniel.calculator.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import avila.daniel.calculator.databinding.ActivityMainBinding
import avila.daniel.calculator.model.Operator
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CalcActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    private val calcViewModel: CalcViewModel by viewModel()
    private val fakeInject: Unit by inject { parametersOf(this) }

    private var operator: Operator? = null
    private var op1: Float? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fakeInject.apply { }

        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setListener()

        mapOf(
            Pair(binding.button0, 0),
            Pair(binding.button1, 1),
            Pair(binding.button2, 2),
            Pair(binding.button3, 3),
            Pair(binding.button4, 4),
            Pair(binding.button5, 5),
            Pair(binding.button6, 6),
            Pair(binding.button7, 7),
            Pair(binding.button8, 8),
            Pair(binding.button9, 9)
        ).forEach { mapEntry ->
            mapEntry.key.setOnClickListener {
                val stringRes = binding.result.text.toString()
                binding.result.setText(String.format("%s%d", stringRes, mapEntry.value))
            }
        }

        mapOf(
            Pair(binding.buttonAdd, Operator.ADD),
            Pair(binding.buttonDivision, Operator.DIVISION),
            Pair(binding.buttonMultiply, Operator.MULTIPLY),
            Pair(binding.buttonSubstract, Operator.SUBSTRACT)
        ).forEach { mapEntry ->
            mapEntry.key.setOnClickListener {
                if (binding.result.text.toString().isNotEmpty()) {
                    op1 = binding.result.text.toString().toFloat()
                    binding.result.setText("")
                    operator = mapEntry.value
                } else if (mapEntry.value == Operator.SUBSTRACT) {
                    binding.result.setText("-")
                }
            }
        }

        binding.buttonC.setOnClickListener {
            op1 = null
            operator = null
            binding.result.setText("")
        }

        binding.buttonEqual.setOnClickListener {
            operator?.run {
                val resultText = binding.result.text.toString()
                if (resultText.isNotEmpty()) {
                    val op2 = resultText.toFloat()
                    when (this) {
                        Operator.ADD -> calcViewModel.add(op1!!, op2)
                        Operator.DIVISION -> calcViewModel.division(op1!!, op2)
                        Operator.MULTIPLY -> calcViewModel.multiply(op1!!, op2)
                        Operator.SUBSTRACT -> calcViewModel.subtract(op1!!, op2)
                    }
                }
            }
        }
    }

    private fun setListener() {
        calcViewModel.resultLiveData.observe(this, { result ->
            when (result) {
                UiState.DivisionByZero -> {
                    binding.result.setText("Division by Zero")
                }
                is UiState.Result -> {
                    op1 = result.value
                    binding.result.setText("${result.value}")
                }
            }
        })
    }
}
