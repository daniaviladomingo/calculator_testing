package avila.daniel.calculator.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import avila.daniel.calculator.*
import avila.daniel.calculator.dispatcher.IDispatcherProvider
import avila.daniel.calculator.domain.interactor.AddUseCase
import avila.daniel.calculator.domain.interactor.DivisionUseCase
import avila.daniel.calculator.domain.interactor.MultiplyUseCase
import avila.daniel.calculator.domain.interactor.SubtractUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CalcViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()

    @Mock
    lateinit var addUseCase: AddUseCase

    @Mock
    lateinit var divisionUseCase: DivisionUseCase

    @Mock
    lateinit var multiplyUseCase: MultiplyUseCase

    @Mock
    lateinit var subtractUseCase: SubtractUseCase

    @Mock
    lateinit var dispatcherProvider: IDispatcherProvider

    @Mock
    lateinit var calcViewModel: CalcViewModel

    @Before
    fun setUp() {
        Mockito.`when`(dispatcherProvider.io()).thenReturn(coroutineScope.dispatcher)
        Mockito.`when`(dispatcherProvider.ui()).thenReturn(coroutineScope.dispatcher)

        calcViewModel = CalcViewModel(
            addUseCase,
            divisionUseCase,
            multiplyUseCase,
            subtractUseCase,
            dispatcherProvider
        )
    }

    @Test
    fun `should add operands`() {
        Mockito.`when`(addUseCase.execute(operands)).thenReturn(flow { ADD_RESULT })

        calcViewModel.add(OP1, OP2)

        checkDispatcherUses()
        Mockito.verify(addUseCase, Mockito.times(1)).execute(operands)
        Mockito.verifyNoMoreInteractions(addUseCase)
    }

    @Test
    fun `should subtract operands`() {
        Mockito.`when`(subtractUseCase.execute(operands)).thenReturn(flow { SUBTRACT_RESULT })

        calcViewModel.subtract(OP1, OP2)

        checkDispatcherUses()
        Mockito.verify(subtractUseCase, Mockito.times(1)).execute(operands)
        Mockito.verifyNoMoreInteractions(subtractUseCase)
    }

    @Test
    fun `should multiply operands`() {
        Mockito.`when`(multiplyUseCase.execute(operands)).thenReturn(flow { MULTIPLY_RESULT })

        calcViewModel.multiply(OP1, OP2)

        checkDispatcherUses()
        Mockito.verify(multiplyUseCase, Mockito.times(1)).execute(operands)
        Mockito.verifyNoMoreInteractions(multiplyUseCase)
    }

    @Test
    fun `should divide operands`() {
        Mockito.`when`(divisionUseCase.execute(operands)).thenReturn(flow { DIVISION_RESULT })

        calcViewModel.division(OP1, OP2)

        checkDispatcherUses()
        Mockito.verify(divisionUseCase, Mockito.times(1)).execute(operands)
        Mockito.verifyNoMoreInteractions(divisionUseCase)
    }

    private fun checkDispatcherUses(){
        Mockito.verify(dispatcherProvider, Mockito.times(1)).io()
        Mockito.verify(dispatcherProvider, Mockito.times(1)).ui()
    }
}